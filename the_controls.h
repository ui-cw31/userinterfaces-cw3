#ifndef THE_CONTROLS_H
#define THE_CONTROLS_H


#include <QMediaPlayer>
#include <QWidget>

QT_BEGIN_NAMESPACE
class QAbstractButton;
class QAbstractSlider;
class QComboBox;
QT_END_NAMESPACE

class the_controls : public QWidget
{
    Q_OBJECT

public:
    explicit the_controls(QWidget *parent = nullptr);

    QMediaPlayer::State state() const;

public slots:
    void setState(QMediaPlayer::State state);

signals:
    void play();
    void pause();
    void stop();

private slots:
    void playClicked();


private:
    QMediaPlayer::State m_playerState = QMediaPlayer::StoppedState;
    QAbstractButton *m_playButton = nullptr;
    QAbstractButton *m_stopButton = nullptr;

};


#endif // THE_CONTROLS_H
