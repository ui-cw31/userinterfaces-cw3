//
// Created by twak on 11/11/2019.
//

#include "the_player.h"

#include <iostream>
#include "QTime"
using namespace std;
// all buttons have been setup, store pointers here
void ThePlayer::updateLayout(){
    QVBoxLayout* allAlbums = new QVBoxLayout;
    alb->setLayout(allAlbums);

    QVBoxLayout* album1 = new QVBoxLayout;
    QLabel *albumName = new QLabel("Album 1");
    albumName->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Fixed);
    album1->addWidget(albumName);
    album1->addWidget(this->buttonWidget);
    QFont font = albumName->font();
    font.setPointSize(11);
    font.setBold(true);
    albumName->setFont(font);
    albumName->setFrameStyle(QFrame::Panel|QFrame::Raised);
    albumName->setAlignment( Qt::AlignCenter);
    allAlbums->addLayout(album1);

    QVBoxLayout* album2 = new QVBoxLayout;
    QLabel *albumName2 = new QLabel("Album 2");
    albumName2->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Fixed);
    album2->addWidget(albumName2);
    album2->addWidget(this->albumButtons2);
    font = albumName2->font();
    font.setPointSize(11);
    font.setBold(true);
    albumName2->setFont(font);
    albumName2->setFrameStyle(QFrame::Panel|QFrame::Raised);
    albumName2->setAlignment( Qt::AlignCenter);
    allAlbums->addLayout(album2);

    QVBoxLayout* album3 = new QVBoxLayout;
    QLabel *albumName3 = new QLabel("Album 3");
    albumName3->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Fixed);
    album3->addWidget(albumName3);
    album3->addWidget(this->albumButtons3);
    font = albumName3->font();
    font.setPointSize(11);
    font.setBold(true);
    albumName3->setFont(font);
    albumName3->setFrameStyle(QFrame::Panel|QFrame::Raised);
    albumName3->setAlignment( Qt::AlignCenter);
    allAlbums->addLayout(album3);

}

void ThePlayer::setContent(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i) {
    buttons = b;
    infos = i;
    jumpTo(buttons -> at(0) -> info);
}

// change the image and video for one button every one second
void ThePlayer::shuffle() {
    TheButtonInfo* i = & infos -> at (rand() % infos->size() );
//        setMedia(*i->url);
    buttons -> at( updateCount++ % buttons->size() ) -> init( i );
}

void ThePlayer::album(){
    QDialogButtonBox* box = new QDialogButtonBox(Qt::Orientation::Vertical);
    QAbstractButton* b1 = new QPushButton("Album 1");
    QAbstractButton* b2 = new QPushButton("Album 2");
    QAbstractButton* b3 = new QPushButton("Album 3");
    QAbstractButton* exit = new QPushButton("Cancel");
    exit->setMinimumSize(200,50);
    b1->setMinimumSize(200,50);
    b2->setMinimumSize(200,50);
    b3->setMinimumSize(200,50);
    box->addButton(b1,QDialogButtonBox::RejectRole);
    QObject::connect(b1, SIGNAL(clicked()), this, SLOT(addAlbum1()));
    box->addButton(b2,QDialogButtonBox::ActionRole);
    QObject::connect(b2, SIGNAL(clicked()), this, SLOT(addAlbum2()));
    box->addButton(b3,QDialogButtonBox::ActionRole);
    QObject::connect(b3, SIGNAL(clicked()), this, SLOT(addAlbum3()));
    box->addButton(exit,QDialogButtonBox::ActionRole);
    QObject::connect(exit, SIGNAL(clicked()), box, SLOT(close()));
    box->show();
}

void ThePlayer::addAlbum1 (){
    QWidget* add = new QWidget;
    QString fileName = QFileDialog::getOpenFileName(add,
                                                    QObject::tr("Add Video"),"/home", QObject::tr("Video Files (*.mp4 *.MOV *.wmv)"));
    if (fileName!=NULL){
        TheButton* button = new TheButton(this->buttonWidget);
        button->setMinimumHeight(100);
        QVBoxLayout* bname = new QVBoxLayout;
        bname->addWidget(button);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), this, SLOT (jumpTo(TheButtonInfo* ))); // when clicked, tell the player to play.
        buttons->push_back(button);
        this->layout->addLayout(bname);
        for ( int i = 0; i < 6; i++ ) {
            file = videos.at(i).url->toDisplayString();
            string s = file.toStdString();
            s.erase(0,7);
            string b = fileName.toStdString();
            if (s.compare(b)==0){
            button->init(&this->videos.at(i));
            alb->setMinimumHeight(alb->height()+150);
            }
        }
        QUrl* b = button->info->url;
        QString s =b->fileName();
        QLabel *v = new QLabel(s);
        v->setAlignment( Qt::AlignCenter);
        bname->addWidget(v);
    }
}

void ThePlayer::addAlbum2(){
    QWidget* add = new QWidget;
    QString fileName = QFileDialog::getOpenFileName(add,
                                                    QObject::tr("Add Video"),"/home", QObject::tr("Video Files (*.mp4 *.MOV *.wmv)"));
    if (fileName!=NULL){
        TheButton* button = new TheButton(this->albumButtons2);
        button->setMinimumHeight(100);
        QVBoxLayout* bname = new QVBoxLayout;
        bname->addWidget(button);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), this, SLOT (jumpTo(TheButtonInfo* ))); // when clicked, tell the player to play.
        buttons2.push_back(button);
        this->layout2->addLayout(bname);
        for ( int i = 0; i < 6; i++ ) {
            file = videos.at(i).url->toDisplayString();
            string s = file.toStdString();
            s.erase(0,7);
            string b = fileName.toStdString();
            if (s.compare(b)==0){
                button->init(&this->videos.at(i));
                alb->setMinimumHeight(alb->height()+150);
            }
        }
        QUrl* b = button->info->url;
        QString s =b->fileName();
        QLabel *v = new QLabel(s);
        v->setAlignment( Qt::AlignCenter);
        bname->addWidget(v);
    }
}

void ThePlayer::addAlbum3(){
    QWidget* add = new QWidget;
    QString fileName = QFileDialog::getOpenFileName(add,
                                                    QObject::tr("Add Video"),"/home", QObject::tr("Video Files (*.mp4 *.MOV *.wmv)"));
    if (fileName!=NULL){
        TheButton* button = new TheButton(this->albumButtons2);
        button->setMinimumHeight(100);
        QVBoxLayout* bname = new QVBoxLayout;
        bname->addWidget(button);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), this, SLOT (jumpTo(TheButtonInfo* ))); // when clicked, tell the player to play.
        buttons3.push_back(button);
        this->layout3->addLayout(bname);
        for ( int i = 0; i < 6; i++ ) {
            file = videos.at(i).url->toDisplayString();
            string s = file.toStdString();
            s.erase(0,7);
            string b = fileName.toStdString();
            if (s.compare(b)==0){
                button->init(&this->videos.at(i));
                alb->setMinimumHeight(alb->height()+150);
            }
        }
        QUrl* b = button->info->url;
        QString s =b->fileName();
        QLabel *v = new QLabel(s);
        v->setAlignment( Qt::AlignCenter);
        bname->addWidget(v);
    }
}

void ThePlayer::removeVids(){
    QWidget* box2 = new QWidget;
    QVBoxLayout *top = new QVBoxLayout();
    QAbstractButton* b1 = new QPushButton("OK");
    b1->setMinimumSize(200,50);
    QObject::connect(b1, SIGNAL(clicked()), box2, SLOT(close()));
    top->addWidget(new QLabel("Select video to remove"));
    top->addWidget(b1);
    box2->setLayout(top);
    box2->show();
}

void ThePlayer::remove(){
    QDialogButtonBox* box = new QDialogButtonBox(Qt::Orientation::Vertical);
    QAbstractButton* b1 = new QPushButton("Album 1");
    QAbstractButton* b2 = new QPushButton("Album 2");
    QAbstractButton* b3 = new QPushButton("Album 3");
    QAbstractButton* exit = new QPushButton("Cancel");
    exit->setMinimumSize(200,50);
    b1->setMinimumSize(200,50);
    b2->setMinimumSize(200,50);
    b3->setMinimumSize(200,50);
    box->addButton(b1,QDialogButtonBox::RejectRole);
    QObject::connect(b1, SIGNAL(clicked()), this, SLOT(removeVids()));
    box->addButton(b2,QDialogButtonBox::ActionRole);
    QObject::connect(b2, SIGNAL(clicked()), this, SLOT(removeVids()));
    box->addButton(b3,QDialogButtonBox::ActionRole);
    QObject::connect(b3, SIGNAL(clicked()), this, SLOT(removeVids()));
    box->addButton(exit,QDialogButtonBox::ActionRole);
    QObject::connect(exit, SIGNAL(clicked()), box, SLOT(close()));
    box->show();
}


// make the list of videos go over one forward
void ThePlayer::nextVideo() {
    if((updateCount + 4) < infos->size()){
         for (int j = 1 ; j < 5 ; j++ ) {
             TheButtonInfo* i = & infos -> at (updateCount+j);
             buttons -> at((j - 1) % buttons->size() ) -> init( i );
         }
        updateCount++;
   }

}

// make the list of videos go one backwards
void ThePlayer::previousVideo() {
    if(updateCount != 0 ){
        for (int j = -1 ; j < 3 ; j++ ) {
            TheButtonInfo* i = & infos -> at (updateCount + j);
            buttons -> at((j + 1) % buttons->size() ) -> init( i );
        }
        updateCount--;
   }

}

void ThePlayer::playStateChanged (QMediaPlayer::State ms) {
    switch (ms) {
        case QMediaPlayer::State::StoppedState:
            play(); // starting playing again...
            break;
    default:
        break;
    }
}

void ThePlayer::onButtonClicked(){
    switch (pstate) {
    case QMediaPlayer::State::PlayingState:
        pause(); // starting playing again...
        pstate = QMediaPlayer::State::PausedState;
        pButton->setIcon(QIcon(":/play.png"));
        break;
    case QMediaPlayer::State::PausedState:
        setPlaybackRate(1);
        play(); // starting playing again...
        pstate = QMediaPlayer::State::PlayingState;
        pButton->setIcon(QIcon(":/pause.png"));
        break;
    default:
        break;
    }

}

void ThePlayer::volumeSlider()
{
    setVolume(m_volumeSlider->value());
    if(m_volumeSlider->value()==0){
        muteButton->setIcon(QIcon(":/muteicon.png"));
    }else{
        muteButton->setIcon(QIcon(":/sound.png"));
    }
}

void ThePlayer::clickMute(){
    muteButton->setIcon(QIcon(":/muteicon.png"));
    setVolume(0);
    m_volumeSlider->setValue(0);
}

void ThePlayer::fastForward(){
    if (playbackRate()<=1){
        setPlaybackRate(1.5);
    }else if (playbackRate()==1.5){
        setPlaybackRate(2);
    }
}

void ThePlayer::rewind(){
    if (playbackRate()>=0){
        setPlaybackRate(-0.25);
    }else if (playbackRate()==-0.25){
        setPlaybackRate(-0.5);
    }
}

void ThePlayer::goFullscreen(QVideoWidget *vw) {

    ThePlayer *fullscreenPlayer = new ThePlayer();
    fullscreenPlayer->setVideoOutput(vw);

}

void ThePlayer::seek(int seconds) {
    setPosition(seconds*1000);
}

void ThePlayer::durationUpdated(qint64 ms) {
    seekSlider->setMaximum(ms/1000);
}

void ThePlayer::positionChanged(qint64 progress){
    //< Update the slider and duration label progress indicators
    durationLabel->setText(composeDurationLabel(progress/1000, duration()/1000));
    m_seekSlider->setSliderPosition(progress/1000);
}

QString ThePlayer::composeDurationLabel(int prog, int duration){
    QString str = "--:-- / --:--";  //< default string

    if (duration){
        //< Set string to mm:ss or hh:mm:ss depending on the length of the video
        QTime progress(prog/3600, (prog/60)%60, prog%60, 0);
        QTime max(duration/3600, (duration/60)%60, duration%60, 0);
        QString format = duration <= 3600 ? "mm:ss": "hh:mm:ss";
        str = progress.toString(format) + " / " + max.toString(format);
    }
    return str;
}


void ThePlayer::jumpTo (TheButtonInfo* button) {
    setMedia( * button -> url);
    play();
}
