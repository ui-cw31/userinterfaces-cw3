/*
 *
 *    ______
 *   /_  __/___  ____ ___  ___  ____
 *    / / / __ \/ __ `__ \/ _ \/ __ \
 *   / / / /_/ / / / / / /  __/ /_/ /
 *  /_/  \____/_/ /_/ /_/\___/\____/
 *              video for sports enthusiasts...
 *
 *  2811 cw3 : twak
 */

#include <iostream>
#include <QApplication>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QMediaPlaylist>
#include <string>
#include <vector>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>
#include <QtCore/QFileInfo>
#include <QtWidgets/QFileIconProvider>
#include <QDesktopServices>
#include <QImageReader>
#include <QMessageBox>
#include <QSlider>
#include <QProgressBar>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include "the_player.h"
#include "the_button.h"
#include "the_controls.h"
#include "QHeaderView"
#include "QTableWidget"
#include "string.h"
#include "stdlib.h"
#include <QStyle>



using namespace std;

// read in videos and thumbnails to this directory
vector<TheButtonInfo> getInfoIn (string loc) {

    vector<TheButtonInfo> out =  vector<TheButtonInfo>();
    QDir dir(QString::fromStdString(loc) );
    QDirIterator it(dir);

    while (it.hasNext()) { // for all files

        QString f = it.next();

            if (f.contains("."))

#if defined(_WIN32)
            if (f.contains(".wmv"))  { // windows
#else
            if (f.contains(".mp4") || f.contains("MOV"))  { // mac/linux
#endif

            QString thumb = f.left( f .length() -4) +".png";
            if (QFile(thumb).exists()) { // if a png thumbnail exists
                QImageReader *imageReader = new QImageReader(thumb);
                    QImage sprite = imageReader->read(); // read the thumbnail
                    if (!sprite.isNull()) {
                        QIcon* ico = new QIcon(QPixmap::fromImage(sprite)); // voodoo to create an icon for the button
                        QUrl* url = new QUrl(QUrl::fromLocalFile( f )); // convert the file location to a generic url
                        out . push_back(TheButtonInfo( url , ico  ) ); // add to the output list
                    }
                    else
                        qDebug() << "warning: skipping video because I couldn't process thumbnail " << thumb << endl;
            }
            else
                qDebug() << "warning: skipping video because I couldn't find thumbnail " << thumb << endl;
        }
    }

    return out;
}


int main(int argc, char *argv[]) {

    // let's just check that Qt is operational first
    qDebug() << "Qt version: " << QT_VERSION_STR << endl;

    // create the Qt Application
    QApplication app(argc, argv);

    // collect all the videos in the folder
    // the widget that will show the video
    QVideoWidget *videoWidget = new QVideoWidget;

    // the QMediaPlayer which controls the playback
    ThePlayer *player = new ThePlayer;
    player->setVideoOutput(videoWidget);

    if (argc == 2)
        player->videos = getInfoIn( string(argv[1]) );

    if (player->videos.size() == 0) {

        const int result = QMessageBox::question(
                    NULL,
                    QString("Tomeo"),
                    QString("no videos found! download, unzip, and add command line argument to \"quoted\" file location. Download videos from Tom's OneDrive?"),
                    QMessageBox::Yes |
                    QMessageBox::No );

        switch( result )
        {
        case QMessageBox::Yes:
          QDesktopServices::openUrl(QUrl("https://leeds365-my.sharepoint.com/:u:/g/personal/scstke_leeds_ac_uk/EcGntcL-K3JOiaZF4T_uaA4BHn6USbq2E55kF_BTfdpPag?e=n1qfuN"));
          break;
        default:
            break;
        }
        exit(-1);
    }

    QWidget window;
    QVBoxLayout *top = new QVBoxLayout();
    window.setLayout(top);
    window.setWindowTitle("tomeo");
    window.setMinimumSize(800, 680);


    QObject::connect(player, &QMediaPlayer::durationChanged, player->m_seekSlider, &QSlider::setMaximum);
    QObject::connect(player, &QMediaPlayer::positionChanged, player->m_seekSlider, &QSlider::setValue);
    QObject::connect(player->m_seekSlider, &QSlider::sliderMoved, player, &QMediaPlayer::setPosition);

    //adding a fullscreen button
    QPushButton *fullscreenButton = new QPushButton();
    fullscreenButton->setIcon(QIcon(":/fullscreen.png"));

    QVBoxLayout* pl = new QVBoxLayout; // Player and video controls
    pl->addWidget(videoWidget);
    QHBoxLayout* playback = new QHBoxLayout;
    QHBoxLayout* volumeControl = new QHBoxLayout;
    // play/pause button

    player->pButton->setIcon(QIcon(":/pause.png"));
    QObject::connect(player->pButton, SIGNAL(clicked()), player, SLOT(onButtonClicked()));
    //  Mute button
    QObject::connect(player->muteButton, SIGNAL(clicked()), player, SLOT(clickMute()));
    if(player->m_volumeSlider->value()==0){
        player->muteButton->setIcon(QIcon(":/muteicon.png"));
    }else{
        player->muteButton->setIcon(QIcon(":/sound.png"));
    }
    //Button for the next video

    player->ffButton->setIcon(QIcon(":/fforward.png"));
    QObject::connect(player->ffButton, SIGNAL(clicked()), player, SLOT(fastForward()));
    //Button for the previous video
    //QPushButton *rewind = new QPushButton();
    player->rewindbutton->setIcon(QIcon(":/rewind.png"));
    QObject::connect(player->rewindbutton, SIGNAL(clicked()), player, SLOT(rewind()));

    QPushButton* add = new QPushButton("Add Video");
    QObject::connect(add, SIGNAL(clicked()), player, SLOT(album()));
    add->setStyleSheet("background-color: #90ee90");


    QPushButton* remove = new QPushButton("Remove Video");
    QObject::connect(remove, SIGNAL(clicked()), player, SLOT(remove()));
    remove->setStyleSheet("background-color: #ff726f");

    //QHBoxLayout* addRemove = new QHBoxLayout;
    //addRemove->addWidget(add);
    //addRemove->addWidget(remove);

    playback->addWidget(add);
    playback->addWidget(remove);

    volumeControl->addWidget(player->muteButton);

    //adding empty label to make volume slider shorter
    QLabel *emptyLabel = new QLabel();
    emptyLabel->setMinimumWidth(450);
    emptyLabel->setMaximumWidth(450);
    emptyLabel->setMaximumHeight(10);

    volumeControl->addWidget(player->m_volumeSlider);
    volumeControl->addWidget(emptyLabel);


    //adding the seek and duration label into a layout
    QHBoxLayout* videoPositionControl = new QHBoxLayout;
    videoPositionControl->addWidget(player->rewindbutton);
    videoPositionControl->addWidget(player->pButton);
    videoPositionControl->addWidget(player->ffButton);
    videoPositionControl->addWidget(player->durationLabel);
    videoPositionControl->addWidget(player->m_seekSlider);
    videoPositionControl->addWidget(fullscreenButton);


    vector<TheButton*> buttons;
    player->albumButtons2->setLayout(player->layout2);
    player->albumButtons3->setLayout(player->layout3);
    player->buttonWidget->setLayout(player->layout);
    // create the six buttons
    for ( int i = 0; i < 6; i++ ) {
        TheButton *button;
        if (i < 2){
            button = new TheButton(player->buttonWidget);
            QVBoxLayout* bname = new QVBoxLayout;
            bname->addWidget(button);
            button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo* ))); // when clicked, tell the player to play.
            buttons.push_back(button);
            player->layout->addLayout(bname);
            button->init(&player->videos.at(i));
            QUrl* b = button->info->url;
            QString s =b->fileName();
            QLabel *v = new QLabel(s);
            v->setAlignment( Qt::AlignCenter);
            bname->addWidget(v);
        } else if (i >= 2 && i <4){
            button = new TheButton(player->albumButtons2);
            QVBoxLayout* bname = new QVBoxLayout;
            bname->addWidget(button);
            button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo* ))); // when clicked, tell the player to play.
            player->buttons2.push_back(button);
            player->layout2->addLayout(bname);
            button->init(&player->videos.at(i));
            QUrl* b = button->info->url;
            QString s =b->fileName();
            QLabel *v = new QLabel(s);
            v->setAlignment( Qt::AlignCenter);
            bname->addWidget(v);
        } else if (i >= 4){
            button = new TheButton(player->albumButtons3);
            QVBoxLayout* bname = new QVBoxLayout;
            bname->addWidget(button);
            button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo* ))); // when clicked, tell the player to play.
            player->buttons3.push_back(button);
            player->layout3->addLayout(bname);
            button->init(&player->videos.at(i));
            QUrl* b = button->info->url;
            QString s =b->fileName();
            QLabel *v = new QLabel(s);
            v->setAlignment( Qt::AlignCenter);
            bname->addWidget(v);
        }
    }

    player->updateLayout();
    QVBoxLayout* scrollandbuttons = new QVBoxLayout;
    QScrollArea* scroll = new QScrollArea;
    scroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    scroll->setWidget(player->alb);
    scroll->setMinimumWidth(250);
    scroll->setMaximumWidth(250);
    scrollandbuttons->addWidget(scroll);
    scrollandbuttons->addLayout(playback);


    pl->addLayout(videoPositionControl);
    pl->addLayout(volumeControl);

    QHBoxLayout* playerscroll = new QHBoxLayout;
    playerscroll->addLayout(pl);
    playerscroll->addLayout(scrollandbuttons);


    // tell the player what buttons and videos are available
    player->setContent(&buttons, & player->videos);

    // create the main window and layout


    // add the video and the buttons to the top level widget
    top->addLayout(playerscroll);

    //top->addLayout(addRemove);

    // showtime!
    window.show();
    // wait for the app to terminate
    return app.exec();

}
