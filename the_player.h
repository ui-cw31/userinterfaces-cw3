//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_PLAYER_H
#define CW2_THE_PLAYER_H


#include <QApplication>
#include <QMediaPlayer>
#include "the_button.h"
#include <vector>
#include <QTimer>
#include <QSlider>
#include <QFileDialog>
#include <QDialogButtonBox>
#include <QLayout>
#include <QLabel>
#include <QScrollArea>

using namespace std;

class ThePlayer : public QMediaPlayer {

Q_OBJECT

private:
    vector<TheButtonInfo>* infos;
    vector<TheButton*>* buttons;
    QTimer* mTimer;
    long updateCount = 0;
    QMediaPlayer::State pstate = QMediaPlayer::PlayingState;


public:
    QLabel *durationLabel = nullptr;
    QSlider *seekSlider = nullptr;
    QSlider *m_volumeSlider = new QSlider(Qt::Horizontal);
    QString file=NULL;
    QWidget* n = new QWidget;
    QWidget* albumButtons2 = new QWidget;
    QWidget* albumButtons3 = new QWidget;
    QBoxLayout *layout2 = new QVBoxLayout();
    QBoxLayout *layout3 = new QVBoxLayout();
    // a row of buttons
    QWidget *buttonWidget = new QWidget();
    // a list of the buttons
    vector<TheButton*> buttons2;
    vector<TheButton*> buttons3;
    // the buttons are arranged horizontally
    QBoxLayout *layout = new QVBoxLayout();
    vector<TheButtonInfo> videos;
    QWidget* alb = new QWidget;
    qint64 m_duration;
    QSlider *m_seekSlider = new QSlider();

    QPushButton *pButton = new QPushButton();
    QPushButton *muteButton = new QPushButton();
    QPushButton *ffButton = new QPushButton();
    QPushButton *rewindbutton = new QPushButton();



    ThePlayer() : QMediaPlayer(NULL) {
        //setVolume(20); // be slightly less annoying
        connect (this, SIGNAL (stateChanged(QMediaPlayer::State)), this, SLOT (playStateChanged(QMediaPlayer::State)) );
        m_volumeSlider->setRange(0,100);
        //initially the volume is set to 0 to minimize the chances of me losing my mind
        setVolume(0);
        m_volumeSlider->setValue(0);
        connect(m_volumeSlider,SIGNAL(valueChanged(int)),this,SLOT(volumeSlider()));
        //setting the seek Slider to horizonal
        m_seekSlider->setOrientation(Qt::Horizontal);


        connect(m_seekSlider, SIGNAL(sliderMoved(int)), this, SLOT(seek(int)));
        durationLabel = new QLabel("--:--/--:--");
        connect(this, SIGNAL(durationChanged(qint64)), this, SLOT(durationUpdated(qint64)));
        connect(this, SIGNAL(positionChanged(qint64)), this, SLOT(positionChanged(qint64)));




    }

    // all buttons have been setup, store pointers here
    void setContent(vector<TheButton*>* b, vector<TheButtonInfo>* i);
    void updateDurationInfo(qint64 currentInfo);
    void updateLayout();
    void durationUpdated(qint64 ms);



private slots:

    // change the image and video for one button every one second
    void shuffle();
    void volumeSlider();
    void playStateChanged (QMediaPlayer::State ms);
    void clickMute();
    void nextVideo();
    void previousVideo();
    void fastForward();
    void rewind();
    void addAlbum1();
    void addAlbum2();
    void addAlbum3();
    void album();
    void remove();
    void removeVids();
    void goFullscreen(QVideoWidget *vw);
    void seek(int seconds);
    void positionChanged(qint64 progress);
    QString composeDurationLabel(int prog, int duration);




public slots:

    // start playing this ButtonInfo
    void jumpTo (TheButtonInfo* button);
    void onButtonClicked ();

};
#endif //CW2_THE_PLAYER_H
